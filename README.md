<h1 align="center">IMDB Ratings, Reviews, and Where to Watch the Best Movies ...</h1>

## Layout:

![](https://codenation-challenges.s3-us-west-1.amazonaws.com/react-11/image.png)

## Objetivo:
The goal was to develop the home page of a movie review website using only **HTML5** e **CSS3**.

## Tools:
- **HTML5**;
- **CSS3**;
- **FLEX-BOX**;
- **CSS GRID LAYOUT**;
- **JAVASCRIPT**;
- **ANIMATE CSS**;
- **WOW JS**;

## Test on your machine:

## First Step:
> - Clone this repository to your machine.

## Second Step:
> - Open the folder in your preferred terminal or code editor.

## Third Step: 
> - Run the following command on your terminal:
    **yarn**

## Fourth Step:

> - After downloading all the dependencies, run the following command in your terminal:
    **gulp start**
    
## Final Step:

> - Have a good time! The project will be running at http://localhost:3000/

Made by **wiriyamu**.

**Towards the Top!**



